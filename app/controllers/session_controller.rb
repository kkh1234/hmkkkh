class SessionController < ApplicationController
    def index
        
        if session[:user]
            
            @uhw = UserHasWorkspace.where(:workspace_id => session[:user]["currentworkspace"])
            @group = Group.where(:workspace_id => session[:user]["currentworkspace"])
        end
       
        render template:"home/index"
    end
    def login
        render template:"session/login"
    end
    def create
        user = User.find_by(email: params[:email].downcase)
        workspaces = Workspace.where(:owner => user.id)
        
        if user && user.authenticate(params[:password])
            #userhw = Userhasworkspace.find_by(user_id:user.id)
            #user.currentworkspace = userhw.workspace_id
            #user.save  
            
            #uhw = Userhasworkspace.where(:workspace_id=> user.currentworkspace)
            session[:workspaces] = workspaces
            session[:user_id] = user.id
            session[:user_name] = user.name
            session[:user]= user
            #session[:uhw] = uhw
            redirect_to "/"
        else
            render html:"Not success"
        end

    end
    def destroy
        session.delete(:user_id)
        session.delete(:user)
        redirect_to "/"
    end
end
