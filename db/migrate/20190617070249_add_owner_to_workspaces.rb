class AddOwnerToWorkspaces < ActiveRecord::Migration[5.2]
  def change
    add_column :workspaces, :owner, :integer
  end
end
