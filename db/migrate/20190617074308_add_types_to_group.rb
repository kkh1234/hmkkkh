class AddTypesToGroup < ActiveRecord::Migration[5.2]
  def change
    add_column :groups, :types, :string
  end
end
